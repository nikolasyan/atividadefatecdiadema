const calcularDesconto = require('./script');

test('calcula o desconto corretamente', () => {
  const preco = 100;
  const desconto = 10;
  const resultadoEsperado = 90;
  expect(calcularDesconto(preco, desconto)).toBe(resultadoEsperado);
});

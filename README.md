# AtividadeFatecDiadema


Em GitLab, um pipeline é uma série de etapas automatizadas que são executadas sempre que você envia código para o repositório. Essas etapas incluem coisas como compilar o código, executar testes automatizados, fazer análise estática, e, finalmente, implantar a aplicação em um ambiente de produção ou de teste.
<br>
<br>

O objetivo principal de um pipeline é alcançar a Integração Contínua (CI) e a Implantação Contínua (CD). CI se refere à prática de integrar código de diferentes desenvolvedores em um repositório compartilhado várias vezes ao dia. CD, por outro lado, implica que, além de integrar o código de forma contínua, ele também é implantado automaticamente em ambientes de teste ou produção.


## Sobre o teste

<p>O Jest fornece o preço do produto como 100 e o percentual de desconto como 10% e espera como resultado 90.</p>
<p>Alterando o return no arquivo script.js e forçando o erro, o Jest ao realizar o teste detecta o erro. Informando o esperado e recebido.</p>

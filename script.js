function calcularDesconto(preco, desconto) {
    return preco - (preco * (desconto / 100));
}

module.exports = calcularDesconto;
